<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailReadCheckTable extends Migration
{
    public function up()
    {
        Schema::create(config('mailread.table'),function(Blueprint $t)
        {
            $t->increments('mail_id')->unsigned();
            $t->string('mail_recipient');
            $t->string('mail_key',16)->unique();
            $t->boolean('mail_read')->default(0);
            $t->timestamp('sent_at');
        });
    }

    public function down()
    {
        Schema::drop('mail_read_list');
    }
}
