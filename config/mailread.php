<?php

return [
    /*
    |--------------------------
    |   Default MailRead table.
    |--------------------------
    */
    'table' => 'mail_read_list',


    /*
    |--------------------------------------------------------
    |   Set an email from which the tracked mails to be send.
    |--------------------------------------------------------
    */
    'send_from' => [
        'name' => 'MailRead Lib',
        'address' => 'no-reply@example.com',
    ],


    /*
    |-------------------------------------------------------
    |   Path to asset file (img) which to be fetched from
    |   the app server in order to mark the mail as read.
    |   Files are pulled with Storage Facade
    |-------------------------------------------------------
     */
    'track_media' => 'logo.png'

];
