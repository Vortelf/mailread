<?php
namespace Vortelf\MailRead\Middleware;

use \Vortelf\MailRead\MailRead as MailRead;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Closure;

class MailReadMiddleware {
    
    public function handle($request, Closure $next)
    {
        try{
            $mail = MailRead::withKey($request->mailkey);
        //} catch(\Exception $e) {
        } catch(ModelNotFoundException $e) {
            if(empty($request->mailkey))
                return response()->json(['message' => 'No Key Provided.'], 401);
            return response()->json(['message' => "mailkey: '".$request->mailkey."' is not valid."], 401);
        }
        $mail->read();
 
        return $next($request);
    }
}
