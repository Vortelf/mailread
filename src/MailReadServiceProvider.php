<?php
namespace Vortelf\MailRead;

use Illuminate\Support\ServiceProvider;

class MailReadServiceProvider extends ServiceProvider
{
    /*
     * Register package service
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();
    }

    /**
     * Perform post-register boot of service.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
        $this->publishMigrations();
        $this->publishViews();
    
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $router = $this->app['router'];
        $router->middleware('mailread', 'Mailread\Middleware\MailReadMiddleware');
        $router->pushMiddlewareToGroup('web', MailRead\Middleware\MailRead::class);
 
    }

    private function mergeConfig()
    {
        $path = $this->getConfigPath();
        $this->mergeConfigFrom($path, 'mailread');
    }

    private function publishConfig()
    {
        $path = $this->getConfigPath();
        $this->publishes([$path => config_path('mailread.php')], 'config');
    }

    private function publishMigrations()
    {
        $path = $this->getMigrationsPath();
        $this->publishes([$path => database_path('migrations')], 'migrations');
    }

    private function publishViews()
    {
        $path = $this->getViewsPath();
        $this->publishes([$path => resource_path('views')], 'views');
    }

    private function getConfigPath()
    {
        return __DIR__.'/../config/mailread.php';
    }

    private function getMigrationsPath()
    {
        return __DIR__.'/../database/migrations/';
    }

    private function getViewsPath()
    {
        return __DIR__.'/views/';
    }
}
