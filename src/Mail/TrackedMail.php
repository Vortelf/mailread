<?php
namespace Vortelf\MailRead\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Vortelf\MailRead\MailRead;

class TrackedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mailkey, $actualMailView, $actualMailData, $sender;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailkey, $actualMailView, $actualMailData, $sender = null)
    {
        $this->mailkey = $mailkey;

        if( view()->exists($actualMailView)) {
            $this->actualMailView = $actualMailView;
        } else {
            throw new \Exception("View not found.");
        }

        $this->actualMailData = $actualMailData;

        $this->sender = $sender;
    }

    /*
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        return $this->from( $this->sender? $this>sender : config('mailread.send_from.address'))
                    ->view('mailread.default')
                    ->with(['mailkey' => $this->mailkey,'actualMailContent' => $this->actualMailView, 'actualMailData' => $this->actualMailData]);
    }
}
