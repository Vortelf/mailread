<?php
namespace Vortelf\MailRead;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class MailRead extends Eloquent
{
    protected $primaryKey = 'mail_id';

    protected $fillable = ['mail_recipient','mail_read'];

    const CREATED_AT = 'sent_at';

    const UPDATED_AT = null;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('mailread.table');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function($mail)
        {
            $mail->mail_read = false;
            $mail->mail_key = str_random(16);
        });
    }
    /* 
     * @param String $mailkey
     *
     * @return MailRead
     */
    public static function withKey($mailkey)
    {
        return (new static)::where('mail_key', $mailkey)->firstOrFail();
    }

    public function getKey()
    {
        return $this->mail_key;
    }

    public function read() {
        $this->update(['mail_read' => True]);
    }

    public function isRead()
    {
        return (boolean)$this->mail_read;
    }
    /*
     * @param String $mailkey
     *
     * @return MailRead
     */
    public function setAsRead($mailkey)
    {
        $mail = $this->where('mail_key', $mailkey)->firstOrFail();
        $mail->read(); 
    }
}
