<?php
namespace Vortelf\MailRead;

use Illuminate\Routing\Controller as Controller;
use Vortelf\MailRead\Mail\TrackedMail;
use Vortelf\MailRead\MailRead;
use Illuminate\Http\Request;
use Mail;

class MailReadController extends Controller
{
    public static function SendMail($send_to, $mail_template = '', $mail_template_data = [])
    {
        $entry = self::createMailReadEntry($send_to);
        Mail::to($send_to)
            ->send(new TrackedMail($entry->mail_key, $mail_template,$mail_template_data));
        return true;
    }

    private static function createMailReadEntry($send_to)
    {
        $mailread = MailRead::create(['mail_recipient' => $send_to]);
        return $mailread;
    }

    public static function MiddlewareAsset(Request $request)
    {
        $content = \Storage::get($request->asset);
        return \Response::make($content, '200')->header('Content-Type', 'image/png');
    }
    
    public static function AssetRoute($asset, $mailkey)
    {
        return route('mailread.middleware',['asset' => $asset, 'mailkey' => $mailkey]);
    }
}
