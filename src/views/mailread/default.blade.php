
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" style="line-height: inherit;">
<head style="line-height: inherit;">
<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" style="line-height: inherit;">
<meta content="width=device-width" name="viewport" style="line-height: inherit;">
<!--[if !mso]><!-->
<meta content="IE=edge" http-equiv="X-UA-Compatible" style="line-height: inherit;">
<!--<![endif]-->
<title style="line-height: inherit;"></title>
<!--[if !mso]><!-->
<!--<![endif]-->
<style type="text/css" style="line-height: inherit;">
		body {
			margin: 0;
			padding: 0;
		}

		table,
		td,
		tr {
			vertical-align: top;
			border-collapse: collapse;
		}

		* {
			line-height: inherit;
		}

		a[x-apple-data-detectors=true] {
			color: inherit !important;
			text-decoration: none !important;
		}

		.ie-browser table {
			table-layout: fixed;
		}

		[owa] .img-container div,
		[owa] .img-container button {
			display: block !important;
		}

		[owa] .fullwidth button {
			width: 100% !important;
		}

		[owa] .block-grid .col {
			display: table-cell;
			float: none !important;
			vertical-align: top;
		}

		.ie-browser .block-grid,
		.ie-browser .num12,
		[owa] .num12,
		[owa] .block-grid {
			width: 700px !important;
		}

		.ie-browser .mixed-two-up .num4,
		[owa] .mixed-two-up .num4 {
			width: 232px !important;
		}

		.ie-browser .mixed-two-up .num8,
		[owa] .mixed-two-up .num8 {
			width: 464px !important;
		}

		.ie-browser .block-grid.two-up .col,
		[owa] .block-grid.two-up .col {
			width: 348px !important;
		}

		.ie-browser .block-grid.three-up .col,
		[owa] .block-grid.three-up .col {
			width: 348px !important;
		}

		.ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
			width: 174px !important;
		}

		.ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
			width: 140px !important;
		}

		.ie-browser .block-grid.six-up .col,
		[owa] .block-grid.six-up .col {
			width: 116px !important;
		}

		.ie-browser .block-grid.seven-up .col,
		[owa] .block-grid.seven-up .col {
			width: 100px !important;
		}

		.ie-browser .block-grid.eight-up .col,
		[owa] .block-grid.eight-up .col {
			width: 87px !important;
		}

		.ie-browser .block-grid.nine-up .col,
		[owa] .block-grid.nine-up .col {
			width: 77px !important;
		}

		.ie-browser .block-grid.ten-up .col,
		[owa] .block-grid.ten-up .col {
			width: 60px !important;
		}

		.ie-browser .block-grid.eleven-up .col,
		[owa] .block-grid.eleven-up .col {
			width: 54px !important;
		}

		.ie-browser .block-grid.twelve-up .col,
		[owa] .block-grid.twelve-up .col {
			width: 50px !important;
		}
	</style>
<style id="media-query" type="text/css" style="line-height: inherit;">
		@media only screen and (min-width: 720px) {
			.block-grid {
				width: 700px !important;
			}

			.block-grid .col {
				vertical-align: top;
			}

			.block-grid .col.num12 {
				width: 700px !important;
			}

			.block-grid.mixed-two-up .col.num3 {
				width: 174px !important;
			}

			.block-grid.mixed-two-up .col.num4 {
				width: 232px !important;
			}

			.block-grid.mixed-two-up .col.num8 {
				width: 464px !important;
			}

			.block-grid.mixed-two-up .col.num9 {
				width: 522px !important;
			}

			.block-grid.two-up .col {
				width: 350px !important;
			}

			.block-grid.three-up .col {
				width: 233px !important;
			}

			.block-grid.four-up .col {
				width: 175px !important;
			}

			.block-grid.five-up .col {
				width: 140px !important;
			}

			.block-grid.six-up .col {
				width: 116px !important;
			}

			.block-grid.seven-up .col {
				width: 100px !important;
			}

			.block-grid.eight-up .col {
				width: 87px !important;
			}

			.block-grid.nine-up .col {
				width: 77px !important;
			}

			.block-grid.ten-up .col {
				width: 70px !important;
			}

			.block-grid.eleven-up .col {
				width: 63px !important;
			}

			.block-grid.twelve-up .col {
				width: 58px !important;
			}
		}

		@media (max-width: 720px) {

			.block-grid,
			.col {
				min-width: 320px !important;
				max-width: 100% !important;
				display: block !important;
			}

			.block-grid {
				width: 100% !important;
			}

			.col {
				width: 100% !important;
			}

			.col>div {
				margin: 0 auto;
			}

			img.fullwidth,
			img.fullwidthOnMobile {
				max-width: 100% !important;
			}

			.no-stack .col {
				min-width: 0 !important;
				display: table-cell !important;
			}

			.no-stack.two-up .col {
				width: 50% !important;
			}

			.no-stack .col.num4 {
				width: 33% !important;
			}

			.no-stack .col.num8 {
				width: 66% !important;
			}

			.no-stack .col.num4 {
				width: 33% !important;
			}

			.no-stack .col.num3 {
				width: 25% !important;
			}

			.no-stack .col.num6 {
				width: 50% !important;
			}

			.no-stack .col.num9 {
				width: 75% !important;
			}

			.video-block {
				max-width: none !important;
			}

			.mobile_hide {
				min-height: 0px;
				max-height: 0px;
				max-width: 0px;
				display: none;
				overflow: hidden;
				font-size: 0px;
			}

			.desktop_hide {
				display: block !important;
				max-height: none !important;
			}
		}
	</style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #81D4DF;line-height: inherit;">
<style id="media-query-bodytag" type="text/css" style="line-height: inherit;">
@media (max-width: 720px) {
  .block-grid {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
  }
  .col {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
  }
  .col > div {
    margin: 0 auto;
  }
  img.fullwidth {
    max-width: 100%!important;
    height: auto!important;
  }
  img.fullwidthOnMobile {
    max-width: 100%!important;
    height: auto!important;
  }
  .no-stack .col {
    min-width: 0!important;
    display: table-cell!important;
  }
  .no-stack.two-up .col {
    width: 50%!important;
  }
  .no-stack.mixed-two-up .col.num4 {
    width: 33%!important;
  }
  .no-stack.mixed-two-up .col.num8 {
    width: 66%!important;
  }
  .no-stack.three-up .col.num4 {
    width: 33%!important
  }
  .no-stack.four-up .col.num3 {
    width: 25%!important
  }
}
</style>
<div class="container">
    <div class="content">
        @yield('content', View::make($actualMailContent)->with($actualMailData))
    </div>
</div>
<!--[if IE]><div class="ie-browser"><![endif]-->
<table bgcolor="#81D4DF" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed;vertical-align: top;min-width: 320px;margin: 0 auto;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #81D4DF;width: 100%;line-height: inherit;" valign="top" width="100%">
<tbody style="line-height: inherit;">
<tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
<td style="word-break: break-word;vertical-align: top;border-collapse: collapse;line-height: inherit;" valign="top">
<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color:#81D4DF"><![endif]-->
<div style="background-color: #0068A5;line-height: inherit;">
<div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 700px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;line-height: inherit;">
<div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;line-height: inherit;">
<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#0068A5;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:700px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
<!--[if (mso)|(IE)]><td align="center" width="700" style="background-color:transparent;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:30px; padding-bottom:30px;"><![endif]-->
<div class="col num12" style="min-width: 320px;max-width: 700px;display: table-cell;vertical-align: top;line-height: inherit;">
<div style="width: 100% !important;line-height: inherit;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 30px;padding-bottom: 30px;padding-right: 0px;padding-left: 0px;line-height: inherit;">
<!--<![endif]-->
<div align="center" class="img-container center autowidth" style="line-height: inherit;">
<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="" align="center"><![endif]--><img align="center" alt="Image" border="0" class="center autowidth" src="{{ Vortelf\MailRead\MailReadController::AssetRoute( !empty(config('mailread.track_media'))? config('mailread.track_media'):'logo.png' ,$mailkey)}}" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;border: 0;height: auto;float: none;width: 100%;max-width: 100px;display: block;line-height: inherit;" title="Image" width="100">
<!--[if mso]></td></tr></table><![endif]-->
</div>
<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
<div style="color:#FFFFFF;font-family:'Poppins', Arial, Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
<div style="font-size: 12px; line-height: 14px; font-family: 'Poppins', Arial, Helvetica, sans-serif; color: #FFFFFF;">
<p style="font-size: 14px; line-height: 16px; text-align: center; margin: 0;">{{ config('app.name') }}</p>
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
</td>
</tr>
</tbody>
</table>
<!--[if (IE)]></div><![endif]-->
</body>
</html>
