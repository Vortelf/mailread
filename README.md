## Laravel Mail Read Checker

Package for chekcing if sent mail has been opened via local middleware.

### Installation

```bash
composer require vortelf/mailread
```

If you use Laravel 5.5 or higher you may skip this step.

Include the service provider within your `config/app.php` file.

```php
'providers' => [
    Vortelf\MialRead\MailReadServiceProvider::class,
]
```

Publish the configuration.

```bash
php artisan vendor:publish --provider="Vortelf\MailRead\MailReadServiceProvider"
```

Then open the `config/mailread.php` and configure sending mail info and default attachment.

Asset files are pulled from `storage/app` //To Fix 

You can edit the default view in `resource/views/mailread/default.blade.php`, which is supposed to be the footer of the email.

### Requirements

This package requires php `7.0` or higher and Laravel `5.2` or higher. It was tested with Laravel `5.2` and higher.

### Usage

```
...
Use Vortelf\MailRead\MailReadController as MRC;
...

MCR::SendMail(String $mail_recipient, String $mail_template, Array $mail_template_data);

```
Example `Example MCR::SendMail('example@mail.com', 'mail.welcome', ['name' => 'John']);`

This method creates new entry of type `MailRead` which stores details about the sent mail including it's read status.

`MailRead` model has these methods:

`public isRead(String $key) Boolean // Checks mail status`

`public getKey() String // Returns the key sent with the mail`

`public static withKey(String $key) // Returns MailRead Model`

`public setAsRead(String $key) // Changes MailRead to "Read"`


### TODO

- Add `read_count` to `MailRead` model

- Add method `findByRecipient`
